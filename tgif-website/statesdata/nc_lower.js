let nc_legis_lower = {
  [{
    "id": "NCL000352",
    "leg_id": "NCL000352",
    "all_ids": ["NCL000352"],
    "full_name": "William O. Richardson",
    "first_name": "William O.",
    "last_name": "Richardson",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/702.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/702",
    "email": "William.Richardson@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "44",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/702"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "44",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-867-0371",
      "email": null,
      "address": "3694 Glenbarry Pl;Fayetteville, NC 28314",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5601",
      "email": "William.Richardson@ncleg.net",
      "address": "16 West Jones Street, Rm. 1021;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:07",
    "updated_at": "2019-01-24 06:13:17"
  }, {
    "id": "NCL000133",
    "leg_id": "NCL000133",
    "all_ids": ["NCL000133", "NCL000314"],
    "full_name": "Grier Martin",
    "first_name": "Grier",
    "last_name": "Martin",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/487.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/487",
    "email": "Grier.Martin@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "34",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/487"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "34",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5773",
      "email": null,
      "address": "16 West Jones Street, Rm. 1023;Raleigh, NC 27601-1096",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5773",
      "email": "Grier.Martin@ncleg.net",
      "address": "16 West Jones Street, Rm. 1023;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:07",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "NCL000399",
    "leg_id": "NCL000399",
    "all_ids": ["NCL000399"],
    "full_name": "Derwin L. Montgomery",
    "first_name": "Derwin L.",
    "last_name": "Montgomery",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/734.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/734",
    "email": "Derwin.Montgomery@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "72",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/734"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "72",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-245-1088",
      "email": null,
      "address": "2021 New Walkertown Rd;Winston-Salem, NC 27101",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5829",
      "email": "Derwin.Montgomery@ncleg.net",
      "address": "16 West Jones Street, Rm. 1006;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:07",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000313",
    "leg_id": "NCL000313",
    "all_ids": ["NCL000313"],
    "full_name": "Josh Dobson",
    "first_name": "Josh",
    "last_name": "Dobson",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/681.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/681",
    "email": "Josh.Dobson@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "85",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/681"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "85",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5862",
      "email": null,
      "address": "300 N. Salisbury Street, Rm. 307B;Raleigh, NC 27603-5925",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5862",
      "email": "Josh.Dobson@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 307B;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:08",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "NCL000090",
    "leg_id": "NCL000090",
    "all_ids": ["NCL000090"],
    "full_name": "Jean Farmer-Butterfield",
    "first_name": "Jean",
    "last_name": "Farmer-Butterfield",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/379.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/379",
    "email": "Jean.Farmer-Butterfield@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "24",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/379"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "24",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5898",
      "email": null,
      "address": "PO Box 2962;Wilson, NC 27894",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5898",
      "email": "Jean.Farmer-Butterfield@ncleg.net",
      "address": "16 West Jones Street, Rm. 1220;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:08",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000205",
    "leg_id": "NCL000205",
    "all_ids": ["NCL000205"],
    "full_name": "Ken Goodman",
    "first_name": "Ken",
    "last_name": "Goodman",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/614.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/614",
    "email": "Ken.Goodman@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "66",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/614"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "66",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-997-2712",
      "email": null,
      "address": "832 Williamsburg Dr;Rockingham, NC 28379",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5823",
      "email": "Ken.Goodman@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 542;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:08",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000275",
    "leg_id": "NCL000275",
    "all_ids": ["NCL000275"],
    "full_name": "Carla D. Cunningham",
    "first_name": "Carla D.",
    "last_name": "Cunningham",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/642.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/642",
    "email": "Carla.Cunningham@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "106",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/642"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "106",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-509-2939",
      "email": null,
      "address": "1400 Sansberry Rd;Charlotte, NC 28262",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5807",
      "email": "Carla.Cunningham@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 609;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:08",
    "updated_at": "2019-01-24 06:13:28"
  }, {
    "id": "NCL000209",
    "leg_id": "NCL000209",
    "all_ids": ["NCL000209"],
    "full_name": "Kelly E. Hastings",
    "first_name": "Kelly E.",
    "last_name": "Hastings",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/618.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/618",
    "email": "Kelly.Hastings@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "110",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/618"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "110",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-473-3468",
      "email": null,
      "address": "PO Box 488;Cherryville, NC 28021",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-2002",
      "email": "Kelly.Hastings@ncleg.net",
      "address": "16 West Jones Street, Rm. 2208;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:09",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000223",
    "leg_id": "NCL000223",
    "all_ids": ["NCL000223"],
    "full_name": "John A. Torbett",
    "first_name": "John A.",
    "last_name": "Torbett",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/606.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/606",
    "email": "John.Torbett@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "108",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/606"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "108",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-263-9282",
      "email": null,
      "address": "210 Blueridge Dr;Stanley, NC 28164",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5868",
      "email": "John.Torbett@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 538;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:09",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000369",
    "leg_id": "NCL000369",
    "all_ids": ["NCL000369"],
    "full_name": "Larry C. Strickland",
    "first_name": "Larry C.",
    "last_name": "Strickland",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/727.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/727",
    "email": "Larry.Strickland@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "28",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/727"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "28",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-632-3200",
      "email": null,
      "address": "PO Box 700;Pine Level, NC 27568",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5849",
      "email": "Larry.Strickland@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 533;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:09",
    "updated_at": "2019-01-24 06:13:33"
  }, {
    "id": "NCL000379",
    "leg_id": "NCL000379",
    "all_ids": ["NCL000379"],
    "full_name": "Terry E. Garrison",
    "first_name": "Terry E.",
    "last_name": "Garrison",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/718.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/718",
    "email": "Terry.Garrison@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "32",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/718"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "32",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-492-7761",
      "email": null,
      "address": "PO Box 551;Henderson, NC 27536",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5824",
      "email": "Terry.Garrison@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 610;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:09",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "NCL000215",
    "leg_id": "NCL000215",
    "all_ids": ["NCL000215"],
    "full_name": "Chuck McGrady",
    "first_name": "Chuck",
    "last_name": "McGrady",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/605.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/605",
    "email": "Chuck.McGrady@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "117",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/605"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "117",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-692-3696",
      "email": null,
      "address": "PO Box 723;Hendersonville, NC 28793",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5956",
      "email": "Chuck.McGrady@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 304;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:10",
    "updated_at": "2019-01-24 06:13:23"
  }, {
    "id": "NCL000204",
    "leg_id": "NCL000204",
    "all_ids": ["NCL000204"],
    "full_name": "John Faircloth",
    "first_name": "John",
    "last_name": "Faircloth",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/603.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/603",
    "email": "John.Faircloth@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "62",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/603"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "62",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-841-4137",
      "email": null,
      "address": "4456 Orchard Knob Ln;High Point, NC 27265",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5877",
      "email": "John.Faircloth@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 613;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:10",
    "updated_at": "2019-01-24 06:13:23"
  }, {
    "id": "NCL000203",
    "leg_id": "NCL000203",
    "all_ids": ["NCL000203"],
    "full_name": "Jimmy Dixon",
    "first_name": "Jimmy",
    "last_name": "Dixon",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/613.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/613",
    "email": "Jimmy.Dixon@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "4",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/613"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "4",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-590-1740",
      "email": null,
      "address": "PO Box 222;Warsaw, NC 28398",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3021",
      "email": "Jimmy.Dixon@ncleg.net",
      "address": "16 West Jones Street, Rm. 2226;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:10",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "NCL000356",
    "leg_id": "NCL000356",
    "all_ids": ["NCL000356"],
    "full_name": "Kyle Hall",
    "first_name": "Kyle",
    "last_name": "Hall",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/704.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/704",
    "email": "Kyle.Hall@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "91",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/704"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "91",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-347-8193",
      "email": null,
      "address": "PO Box 2024;King, NC 27021",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5609",
      "email": "Kyle.Hall@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 529;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:10",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "NCL000368",
    "leg_id": "NCL000368",
    "all_ids": ["NCL000368"],
    "full_name": "John Autry",
    "first_name": "John",
    "last_name": "Autry",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/710.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/710",
    "email": "John.Autry@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "100",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/710"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "100",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-537-2735",
      "email": null,
      "address": "PO Box 189113;Charlotte, NC 28218",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-0706",
      "email": "John.Autry@ncleg.net",
      "address": "16 West Jones Street, Rm.1019;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:11",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000075",
    "leg_id": "NCL000075",
    "all_ids": ["NCL000075"],
    "full_name": "Becky Carney",
    "first_name": "Becky",
    "last_name": "Carney",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/322.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/322",
    "email": "Becky.Carney@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "102",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/322"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "102",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5827",
      "email": null,
      "address": "PO Box 32873;Charlotte, NC 28232",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5827",
      "email": "Becky.Carney@ncleg.net",
      "address": "16 West Jones Street, Rm. 1221;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:11",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000288",
    "leg_id": "NCL000288",
    "all_ids": ["NCL000288"],
    "full_name": "Donny Lambeth",
    "first_name": "Donny",
    "last_name": "Lambeth",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/646.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/646",
    "email": "Donny.Lambeth@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "75",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/646"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "75",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5747",
      "email": null,
      "address": "4627 S Main St;Winston-Salem, NC 27127",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5747",
      "email": "Donny.Lambeth@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 303;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:11",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "NCL000363",
    "leg_id": "NCL000363",
    "all_ids": ["NCL000363"],
    "full_name": "John Sauls",
    "first_name": "John",
    "last_name": "Sauls",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/393.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/393",
    "email": "John.Sauls@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "51",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/393"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "51",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-775-8033",
      "email": null,
      "address": "2609 Wellington Dr;Sanford, NC 27330",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3026",
      "email": "John.Sauls@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 408;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:12",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "NCL000116",
    "leg_id": "NCL000116",
    "all_ids": ["NCL000116"],
    "full_name": "Pat B. Hurley",
    "first_name": "Pat B.",
    "last_name": "Hurley",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/560.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/560",
    "email": "Pat.Hurley@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "70",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/560"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "70",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-625-9210",
      "email": null,
      "address": "141 Ridgecrest Rd;Asheboro, NC 27203",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5865",
      "email": "Pat.Hurley@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 532;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:12",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000108",
    "leg_id": "NCL000108",
    "all_ids": ["NCL000108"],
    "full_name": "Pricey Harrison",
    "first_name": "Pricey",
    "last_name": "Harrison",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/504.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/504",
    "email": "Pricey.Harrison@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "61",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/504"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "61",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-274-5574",
      "email": null,
      "address": "PO Box 9339;Greensboro, NC 27429-9339",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5771",
      "email": "Pricey.Harrison@ncleg.net",
      "address": "16 West Jones Street, Rm. 1218;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:12",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "NCL000228",
    "leg_id": "NCL000228",
    "all_ids": ["NCL000228"],
    "full_name": "Jason Saine",
    "first_name": "Jason",
    "last_name": "Saine",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/632.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/632",
    "email": "Jason.Saine@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "97",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/632"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "97",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-479-1803",
      "email": null,
      "address": "1760 Whispering Pines Dr;Lincolnton, NC 28092",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5782",
      "email": "Jason.Saine@ncleg.net",
      "address": "16 West Jones Street, Rm. 1326;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:13",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "NCL000371",
    "leg_id": "NCL000371",
    "all_ids": ["NCL000371"],
    "full_name": "Holly Grange",
    "first_name": "Holly",
    "last_name": "Grange",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/709.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/709",
    "email": "Holly.Grange@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "20",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/709"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "20",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5830",
      "email": null,
      "address": "8209A Market St #294;Wilmington, NC 28411",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5830",
      "email": "Holly.Grange@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 526;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:13",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "NCL000328",
    "leg_id": "NCL000328",
    "all_ids": ["NCL000328"],
    "full_name": "Larry Yarborough",
    "first_name": "Larry",
    "last_name": "Yarborough",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/694.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/694",
    "email": "Larry.Yarborough@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "2",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/694"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "2",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-503-8282",
      "email": null,
      "address": "87 Duck Pointe Dr;Roxboro, NC 27574",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-0850",
      "email": "Larry.Yarborough@ncleg.net",
      "address": "16 West Jones Street, Rm. 2219;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:13",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "NCL000284",
    "leg_id": "NCL000284",
    "all_ids": ["NCL000284"],
    "full_name": "Jon Hardister",
    "first_name": "Jon",
    "last_name": "Hardister",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/645.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/645",
    "email": "Jon.Hardister@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "59",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/645"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "59",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5191",
      "email": null,
      "address": "6427 Bellcross Trail;Whitsett, NC 27377",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5191",
      "email": "Jon.Hardister@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 638;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:13",
    "updated_at": "2019-01-24 06:13:31"
  }, {
    "id": "NCL000274",
    "leg_id": "NCL000274",
    "all_ids": ["NCL000274"],
    "full_name": "Debra Conrad",
    "first_name": "Debra",
    "last_name": "Conrad",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/675.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/675",
    "email": "Debra.Conrad@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "74",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/675"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "74",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-978-0169",
      "email": null,
      "address": "4004 Pemberton Ct;Winston-Salem, NC 27106",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5787",
      "email": "Debra.Conrad@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 635;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:13",
    "updated_at": "2019-01-24 06:13:23"
  }, {
    "id": "NCL000119",
    "leg_id": "NCL000119",
    "all_ids": ["NCL000119"],
    "full_name": "Verla Insko",
    "first_name": "Verla",
    "last_name": "Insko",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/46.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/46",
    "email": "Verla.Insko@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "56",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/46"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "56",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-7208",
      "email": null,
      "address": "610 Surry Rd;Chapel Hill, NC 27514",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-7208",
      "email": "Verla.Insko@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 503;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:14",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000148",
    "leg_id": "NCL000148",
    "all_ids": ["NCL000148"],
    "full_name": "Garland E. Pierce",
    "first_name": "Garland E.",
    "last_name": "Pierce",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/497.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/497",
    "email": "Garland.Pierce@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "48",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/497"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "48",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-369-2844",
      "email": null,
      "address": "21981 Buie St;Wagram, NC 28396",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5803",
      "email": "Garland.Pierce@ncleg.net",
      "address": "16 West Jones Street, Rm. 1204;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:14",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000342",
    "leg_id": "NCL000342",
    "all_ids": ["NCL000342"],
    "full_name": "Howard J. Hunter, III",
    "first_name": "Howard J.",
    "last_name": "Hunter",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/692.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/692",
    "email": "Howard.Hunter@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "5",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/692"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "5",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-332-3189",
      "email": null,
      "address": "PO Box 944;Ahoskie, NC 27910",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5780",
      "email": "Howard.Hunter@ncleg.net",
      "address": "16 West Jones Street, Rm. 2121;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:14",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "NCL000243",
    "leg_id": "NCL000243",
    "all_ids": ["NCL000243"],
    "full_name": "Allen McNeill",
    "first_name": "Allen",
    "last_name": "McNeill",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/635.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/635",
    "email": "Allen.McNeill@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "78",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/635"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "78",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-302-0263",
      "email": null,
      "address": "4172 NC Hwy 49 South;Asheboro, NC 27205",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-4946",
      "email": "Allen.McNeill@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 411;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:14",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "NCL000345",
    "leg_id": "NCL000345",
    "all_ids": ["NCL000345"],
    "full_name": "Lee Zachary",
    "first_name": "Lee",
    "last_name": "Zachary",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/695.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/695",
    "email": "Lee.Zachary@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "73",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/695"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "73",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-677-1777",
      "email": null,
      "address": "PO Box 1780;Yadkinville, NC 27055",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-8361",
      "email": "Lee.Zachary@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 420;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:14",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000306",
    "leg_id": "NCL000306",
    "all_ids": ["NCL000306"],
    "full_name": "Rena W. Turner",
    "first_name": "Rena W.",
    "last_name": "Turner",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/658.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/658",
    "email": "Rena.Turner@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "84",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/658"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "84",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-876-4948",
      "email": null,
      "address": "247 Gethsemane Rd;Olin, NC 28660",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5661",
      "email": "Rena.Turner@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 606;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:15",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000211",
    "leg_id": "NCL000211",
    "all_ids": ["NCL000211"],
    "full_name": "D. Craig Horn",
    "first_name": "D. Craig",
    "last_name": "Horn",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/604.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/604",
    "email": "Craig.Horn@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "68",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/604"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "68",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-844-9960",
      "email": null,
      "address": "5909 Bluebird Hill Ln;Weddington, NC 28104",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-2406",
      "email": "Craig.Horn@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 305;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:15",
    "updated_at": "2019-01-24 06:13:31"
  }, {
    "id": "NCL000339",
    "leg_id": "NCL000339",
    "all_ids": ["NCL000339"],
    "full_name": "Cecil Brockman",
    "first_name": "Cecil",
    "last_name": "Brockman",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/691.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/691",
    "email": "Cecil.Brockman@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "60",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/691"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "60",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5825",
      "email": null,
      "address": "1166 Roberts Ln;High Point, NC 27260",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5825",
      "email": "Cecil.Brockman@ncleg.net",
      "address": "16 West Jones Street, Rm. 2119;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:15",
    "updated_at": "2019-01-24 06:13:33"
  }, {
    "id": "NCL000272",
    "leg_id": "NCL000272",
    "all_ids": ["NCL000272"],
    "full_name": "Dana Bumgardner",
    "first_name": "Dana",
    "last_name": "Bumgardner",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/659.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/659",
    "email": "Dana.Bumgardner@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "109",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/659"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "109",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-861-1648",
      "email": null,
      "address": "3517 Lincoln Ln;Gastonia, NC 28056",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5809",
      "email": "Dana.Bumgardner@ncleg.net",
      "address": "16 West Jones Street, Rm. 1206;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:15",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000344",
    "leg_id": "NCL000344",
    "all_ids": ["NCL000344"],
    "full_name": "Shelly Willingham",
    "first_name": "Shelly",
    "last_name": "Willingham",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/700.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/700",
    "email": "Shelly.Willingham@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "23",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/700"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "23",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-442-8659",
      "email": null,
      "address": "916 Hill St;Rocky Mount, NC 27801",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3024",
      "email": "Shelly.Willingham@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 513;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:15",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "NCL000285",
    "leg_id": "NCL000285",
    "all_ids": ["NCL000285"],
    "full_name": "Yvonne Lewis Holley",
    "first_name": "Yvonne Lewis",
    "last_name": "Holley",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/650.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/650",
    "email": "Yvonne.Holley@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "38",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/650"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "38",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-828-3873",
      "email": null,
      "address": "1505 Tierney Cir;Raleigh, NC 27610",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5758",
      "email": "Yvonne.Holley@ncleg.net",
      "address": "16 West Jones Street, Rm. 1219;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:16",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000365",
    "leg_id": "NCL000365",
    "all_ids": ["NCL000365"],
    "full_name": "Kevin Corbin",
    "first_name": "Kevin",
    "last_name": "Corbin",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/716.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/716",
    "email": "Kevin.Corbin@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "120",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/716"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "120",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-524-7799",
      "email": null,
      "address": "PO Box 758;Franklin, NC 28744",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5859",
      "email": "Kevin.Corbin@ncleg.net",
      "address": "16 West Jones Street, Rm. 2215;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:16",
    "updated_at": "2019-01-24 06:13:33"
  }, {
    "id": "NCL000333",
    "leg_id": "NCL000333",
    "all_ids": ["NCL000333"],
    "full_name": "John A. Fraley",
    "first_name": "John A.",
    "last_name": "Fraley",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/686.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/686",
    "email": "John.Fraley@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "95",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/686"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "95",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "917-854-9793",
      "email": null,
      "address": "1311 Fern Hill Rd;Mooresville, NC 28117",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5741",
      "email": "John.Fraley@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 637;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:16",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "NCL000354",
    "leg_id": "NCL000354",
    "all_ids": ["NCL000354", "NCL000355"],
    "full_name": "Gregory F. Murphy, MD",
    "first_name": "Gregory F.",
    "last_name": "Murphy",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/703.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/703",
    "email": "Gregory.Murphy@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "9",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/703"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "9",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-321-2282",
      "email": null,
      "address": "502 Queen Annes Rd;Greenville, NC 27858",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5757",
      "email": "Gregory.Murphy@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 307B1;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:17",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000387",
    "leg_id": "NCL000387",
    "all_ids": ["NCL000387"],
    "full_name": "Cody Henson",
    "first_name": "Cody",
    "last_name": "Henson",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/720.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/720",
    "email": "Cody.Henson@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "113",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/720"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "113",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-4466",
      "email": null,
      "address": "PO Box 1626;Brevard, NC 28712",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-4466",
      "email": "Cody.Henson@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 537;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:17",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000096",
    "leg_id": "NCL000096",
    "all_ids": ["NCL000096"],
    "full_name": "Rosa U. Gill",
    "first_name": "Rosa U.",
    "last_name": "Gill",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/597.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/597",
    "email": "Rosa.Gill@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "33",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/597"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "33",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-821-0425",
      "email": null,
      "address": "2408 Foxtrot Rd;Raleigh, NC 27610",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5880",
      "email": "Rosa.Gill@ncleg.net",
      "address": "16 West Jones Street, Rm. 1303;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:17",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000391",
    "leg_id": "NCL000391",
    "all_ids": ["NCL000391"],
    "full_name": "Deb Butler",
    "first_name": "Deb",
    "last_name": "Butler",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/730.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/730",
    "email": "Deb.Butler@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "18",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/730"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "18",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-228-9222",
      "email": null,
      "address": "711 Princess St, 2nd Floor;Wilmington, NC 28401",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5754",
      "email": "Deb.Butler@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 502;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:17",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "NCL000332",
    "leg_id": "NCL000332",
    "all_ids": ["NCL000332"],
    "full_name": "John Ager",
    "first_name": "John",
    "last_name": "Ager",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/689.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/689",
    "email": "John.Ager@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "115",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/689"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "115",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-628-2616",
      "email": null,
      "address": "15 Clarke Ln;Fairview, NC 28730",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5746",
      "email": "John.Ager@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 509;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:17",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "NCL000373",
    "leg_id": "NCL000373",
    "all_ids": ["NCL000373"],
    "full_name": "Mary Belk",
    "first_name": "Mary",
    "last_name": "Belk",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/713.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/713",
    "email": "Mary.Belk@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "88",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/713"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "88",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "980-579-2800",
      "email": null,
      "address": "PO Box 33115;Charlotte, NC 28233",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5607",
      "email": "Mary.Belk@ncleg.net",
      "address": "16 West Jones Street, Rm. 1313;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:19",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000389",
    "leg_id": "NCL000389",
    "all_ids": ["NCL000389"],
    "full_name": "David Rogers",
    "first_name": "David",
    "last_name": "Rogers",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/707.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/707",
    "email": "David.Rogers@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "112",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/707"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "112",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5749",
      "email": null,
      "address": "PO Box 469;Rutherfordton, NC 28139",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5749",
      "email": "David.Rogers@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 418C;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:19",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000304",
    "leg_id": "NCL000304",
    "all_ids": ["NCL000304"],
    "full_name": "Evelyn Terry",
    "first_name": "Evelyn",
    "last_name": "Terry",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/676.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/676",
    "email": "Evelyn.Terry@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "71",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/676"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "71",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-788-5008",
      "email": null,
      "address": "1224 Reynolds Forest Dr;Winston-Salem, NC 27107",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5777",
      "email": "Evelyn.Terry@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 514;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:19",
    "updated_at": "2019-01-24 06:13:23"
  }, {
    "id": "NCL000062",
    "leg_id": "NCL000062",
    "all_ids": ["NCL000062"],
    "full_name": "Hugh Blackwell",
    "first_name": "Hugh",
    "last_name": "Blackwell",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/580.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/580",
    "email": "Hugh.Blackwell@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "86",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/580"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "86",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5805",
      "email": null,
      "address": "321 Mountain View Ave SE;Valdese, NC 28690",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5805",
      "email": "Hugh.Blackwell@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 541;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:20",
    "updated_at": "2019-01-24 06:13:33"
  }, {
    "id": "NCL000293",
    "leg_id": "NCL000293",
    "all_ids": ["NCL000293"],
    "full_name": "Michele D. Presnell",
    "first_name": "Michele D.",
    "last_name": "Presnell",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/670.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/670",
    "email": "Michele.Presnell@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "118",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/670"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "118",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-208-3874",
      "email": null,
      "address": "316 Woodstock Dr;Burnsville, NC 28714",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5732",
      "email": "Michele.Presnell@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 306B2;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:20",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000137",
    "leg_id": "NCL000137",
    "all_ids": ["NCL000137"],
    "full_name": "Pat McElraft",
    "first_name": "Pat",
    "last_name": "McElraft",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/570.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/570",
    "email": "Pat.McElraft@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "13",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/570"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "13",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-342-0693",
      "email": null,
      "address": "PO Box 4477;Emerald Isle, NC 28594",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-6275",
      "email": "Pat.McElraft@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 634;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:20",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000076",
    "leg_id": "NCL000076",
    "all_ids": ["NCL000076"],
    "full_name": "George G. Cleveland",
    "first_name": "George G.",
    "last_name": "Cleveland",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/476.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/476",
    "email": "George.Cleveland@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "14",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/476"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "14",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-346-3866",
      "email": null,
      "address": "224 Campbell Place;Jacksonville, NC 28546",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-6707",
      "email": "George.Cleveland@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 417A;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:20",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000338",
    "leg_id": "NCL000338",
    "all_ids": ["NCL000338"],
    "full_name": "Jay Adams",
    "first_name": "Jay",
    "last_name": "Adams",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/697.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/697",
    "email": "Jay.Adams@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "96",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/697"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "96",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5988",
      "email": null,
      "address": "PO Box 217;Hickory, NC 28603",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5988",
      "email": "Jay.Adams@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 301N;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:21",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000392",
    "leg_id": "NCL000392",
    "all_ids": ["NCL000392"],
    "full_name": "MaryAnn Black",
    "first_name": "MaryAnn",
    "last_name": "Black",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/731.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/731",
    "email": "MaryAnn.Black@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "29",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/731"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "29",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5872",
      "email": null,
      "address": "300 N. Salisbury Street, Rm. 501;Raleigh, NC 27603-5925",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5872",
      "email": "MaryAnn.Black@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 501;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:21",
    "updated_at": "2019-01-24 06:13:31"
  }, {
    "id": "NCL000206",
    "leg_id": "NCL000206",
    "all_ids": ["NCL000206"],
    "full_name": "Charles Graham",
    "first_name": "Charles",
    "last_name": "Graham",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/615.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/615",
    "email": "Charles.Graham@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "47",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/615"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "47",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-739-3969",
      "email": null,
      "address": "479 Bee Gee Rd;Lumberton, NC 28358",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-0875",
      "email": "Charles.Graham@ncleg.net",
      "address": "16 West Jones Street, Rm.1309;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:22",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "NCL000276",
    "leg_id": "NCL000276",
    "all_ids": ["NCL000276"],
    "full_name": "Jeffrey Elmore",
    "first_name": "Jeffrey",
    "last_name": "Elmore",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/643.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/643",
    "email": "Jeffrey.Elmore@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "94",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/643"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "94",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5935",
      "email": null,
      "address": "PO Box 522;North Wilkesboro, NC 28659",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5935",
      "email": "Jeffrey.Elmore@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 306A3;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:22",
    "updated_at": "2019-01-24 06:13:28"
  }, {
    "id": "NCL000120",
    "leg_id": "NCL000120",
    "all_ids": ["NCL000120"],
    "full_name": "Darren G. Jackson",
    "first_name": "Darren G.",
    "last_name": "Jackson",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/595.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/595",
    "email": "Darren.Jackson@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "39",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/595"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "39",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5974",
      "email": null,
      "address": "1525 Crickett Rd;Raleigh, NC 27610",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5974",
      "email": "Darren.Jackson@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 506;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:23",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000265",
    "leg_id": "NCL000265",
    "all_ids": ["NCL000265"],
    "full_name": "Dean Arp",
    "first_name": "Dean",
    "last_name": "Arp",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/640.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/640",
    "email": "Dean.Arp@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "69",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/640"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "69",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-282-0418",
      "email": null,
      "address": "PO Box 1511;Monroe, NC 28111-1511",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3007",
      "email": "Dean.Arp@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 307A;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:23",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "NCL000092",
    "leg_id": "NCL000092",
    "all_ids": ["NCL000092"],
    "full_name": "Elmer Floyd",
    "first_name": "Elmer",
    "last_name": "Floyd",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/583.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/583",
    "email": "Elmer.Floyd@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "43",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/583"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "43",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-488-6903",
      "email": null,
      "address": "207 Courtney St;Fayetteville, NC 28301",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5959",
      "email": "Elmer.Floyd@ncleg.net",
      "address": "16 West Jones Street, Rm. 1325;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:24",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "NCL000303",
    "leg_id": "NCL000303",
    "all_ids": ["NCL000303"],
    "full_name": "John Szoka",
    "first_name": "John",
    "last_name": "Szoka",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/662.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/662",
    "email": "John.Szoka@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "45",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/662"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "45",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-583-2960",
      "email": null,
      "address": "6922 Surrey Rd;Fayetteville, NC 28306",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-9892",
      "email": "John.Szoka@ncleg.net",
      "address": "16 West Jones Street, Rm. 2207;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:24",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000317",
    "leg_id": "NCL000317",
    "all_ids": ["NCL000317"],
    "full_name": "Graig R. Meyer",
    "first_name": "Graig R.",
    "last_name": "Meyer",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/683.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/683",
    "email": "Graig.Meyer@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "50",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/683"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "50",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3019",
      "email": null,
      "address": "PO Box 867;Hillsborough, NC 27278",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3019",
      "email": "Graig.Meyer@ncleg.net",
      "address": "16 West Jones Street, Rm. 1017;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:24",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "NCL000386",
    "leg_id": "NCL000386",
    "all_ids": ["NCL000386"],
    "full_name": "Brenden H. Jones",
    "first_name": "Brenden H.",
    "last_name": "Jones",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/723.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/723",
    "email": "Brenden.Jones@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "46",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/723"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "46",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5821",
      "email": null,
      "address": "329 S US Hwy 701;Tabor City, NC 28463",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5821",
      "email": "Brenden.Jones@ncleg.net",
      "address": "16 West Jones Street, Rm. 1227;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:24",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "NCL000179",
    "leg_id": "NCL000179",
    "all_ids": ["NCL000179"],
    "full_name": "Michael H. Wray",
    "first_name": "Michael H.",
    "last_name": "Wray",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/481.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/481",
    "email": "Michael.Wray@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "27",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/481"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "27",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-535-3297",
      "email": null,
      "address": "PO Box 904;Gaston, NC 27832",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5662",
      "email": "Michael.Wray@ncleg.net",
      "address": "16 West Jones Street, Rm. 2123;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:25",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "NCL000320",
    "leg_id": "NCL000320",
    "all_ids": ["NCL000320"],
    "full_name": "Robert T. Reives, II",
    "first_name": "Robert T.",
    "last_name": "Reives",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/684.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/684",
    "email": "Robert.Reives@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "54",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/684"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "54",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-0057",
      "email": null,
      "address": "16 West Jones Street, Rm. 1323;Raleigh, NC 27601-1096",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-0057",
      "email": "Robert.Reives@ncleg.net",
      "address": "16 West Jones Street, Rm. 1323;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:26",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000130",
    "leg_id": "NCL000130",
    "all_ids": ["NCL000130"],
    "full_name": "Marvin W. Lucas",
    "first_name": "Marvin W.",
    "last_name": "Lucas",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/216.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/216",
    "email": "Marvin.Lucas@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "42",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/216"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "42",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-497-2733",
      "email": null,
      "address": "3318 Hedgemoor Cir;Spring Lake, NC 28390",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5775",
      "email": "Marvin.Lucas@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 402;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:27",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000347",
    "leg_id": "NCL000347",
    "all_ids": ["NCL000347"],
    "full_name": "Brian Turner",
    "first_name": "Brian",
    "last_name": "Turner",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/696.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/696",
    "email": "Brian.Turner@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "116",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/696"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "116",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3012",
      "email": null,
      "address": "10 Cedarcliff Rd.;Asheville, NC 28803",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3012",
      "email": "Brian.Turner@ncleg.net",
      "address": "16 West Jones Street, Rm. 1217;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:27",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "NCL000229",
    "leg_id": "NCL000229",
    "all_ids": ["NCL000229", "NCL000230"],
    "full_name": "Larry G. Pittman",
    "first_name": "Larry G.",
    "last_name": "Pittman",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/633.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/633",
    "email": "Larry.Pittman@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "83",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/633"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "83",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-782-3528",
      "email": null,
      "address": "PO Box 5959;Concord, NC 28027",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-2009",
      "email": "Larry.Pittman@ncleg.net",
      "address": "16 West Jones Street, Rm. 1010;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:27",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000296",
    "leg_id": "NCL000296",
    "all_ids": ["NCL000296"],
    "full_name": "Dennis Riddell",
    "first_name": "Dennis",
    "last_name": "Riddell",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/665.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/665",
    "email": "Dennis.Riddell@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "64",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/665"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "64",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-222-1303",
      "email": null,
      "address": "6343 Beale Rd;Snow Camp, NC 27349",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5905",
      "email": "Dennis.Riddell@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 416A;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:28",
    "updated_at": "2019-01-24 06:13:28"
  }, {
    "id": "NCL000390",
    "leg_id": "NCL000390",
    "all_ids": ["NCL000390"],
    "full_name": "Larry W. Potts",
    "first_name": "Larry W.",
    "last_name": "Potts",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/724.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/724",
    "email": "Larry.Potts@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "81",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/724"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "81",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-0873",
      "email": null,
      "address": "373 Waitman Rd;Lexington, NC 27295",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-0873",
      "email": "Larry.Potts@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 306B1;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:28",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "NCL000066",
    "leg_id": "NCL000066",
    "all_ids": ["NCL000066"],
    "full_name": "James L. Boles, Jr.",
    "first_name": "James L.",
    "last_name": "Boles",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/581.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/581",
    "email": "Jamie.Boles@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "52",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/581"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "52",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-692-6262",
      "email": null,
      "address": "425 W Pennsylvania Ave;Southern Pines, NC 28387",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5903",
      "email": "Jamie.Boles@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 528;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:28",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "NCL000245",
    "leg_id": "NCL000245",
    "all_ids": ["NCL000245"],
    "full_name": "Ted Davis, Jr.",
    "first_name": "Ted",
    "last_name": "Davis",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/637.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/637",
    "email": "Ted.Davis@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "19",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/637"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "19",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-763-6249",
      "email": null,
      "address": "PO Box 2535;Wilmington, NC 28402",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5786",
      "email": "Ted.Davis@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 417B;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:29",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "NCL000224",
    "leg_id": "NCL000224",
    "all_ids": ["NCL000224"],
    "full_name": "Harry Warren",
    "first_name": "Harry",
    "last_name": "Warren",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/630.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/630",
    "email": "Harry.Warren@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "76",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/630"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "76",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-603-8898",
      "email": null,
      "address": "201 Kingsbridge Rd;Salisbury, NC 28144",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5784",
      "email": "Harry.Warren@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 611;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:29",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "NCL000069",
    "leg_id": "NCL000069",
    "all_ids": ["NCL000069"],
    "full_name": "William D. Brisson",
    "first_name": "William D.",
    "last_name": "Brisson",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/558.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/558",
    "email": "William.Brisson@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "22",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/558"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "22",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-862-7007",
      "email": null,
      "address": "PO Box 531;Dublin, NC 28332",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5772",
      "email": "William.Brisson@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 405;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:29",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000381",
    "leg_id": "NCL000381",
    "all_ids": ["NCL000381"],
    "full_name": "Amos L. Quick, III",
    "first_name": "Amos L.",
    "last_name": "Quick",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/725.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/725",
    "email": "Amos.Quick@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "58",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/725"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "58",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5902",
      "email": null,
      "address": "300 N. Salisbury Street, Rm. 510;Raleigh, NC 27603-5925",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5902",
      "email": "Amos.Quick@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 510;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:29",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "NCL000117",
    "leg_id": "NCL000117",
    "all_ids": ["NCL000117"],
    "full_name": "Frank Iler",
    "first_name": "Frank",
    "last_name": "Iler",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/598.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/598",
    "email": "Frank.Iler@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "17",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/598"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "17",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-301-1450",
      "email": null,
      "address": "2515 Marsh Hen Dr;Oak Island, NC 28465",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-301-1450",
      "email": "Frank.Iler@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 639;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:30",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "NCL000091",
    "leg_id": "NCL000091",
    "all_ids": ["NCL000091"],
    "full_name": "Susan C. Fisher",
    "first_name": "Susan C.",
    "last_name": "Fisher",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/463.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/463",
    "email": "Susan.Fisher@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "114",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/463"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "114",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-258-5355",
      "email": null,
      "address": "7 Maple Ridge Ln;Asheville, NC 28806",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-2013",
      "email": "Susan.Fisher@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 504;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:30",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "NCL000055",
    "leg_id": "NCL000055",
    "all_ids": ["NCL000055"],
    "full_name": "Kelly M. Alexander, Jr.",
    "first_name": "Kelly M.",
    "last_name": "Alexander",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/579.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/579",
    "email": "Kelly.Alexander@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "107",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/579"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "107",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-333-1167",
      "email": null,
      "address": "1424 Statesville Ave;Charlotte, NC 28206",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5778",
      "email": "Kelly.Alexander@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 404;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:30",
    "updated_at": "2019-01-24 06:13:31"
  }, {
    "id": "NCL000160",
    "leg_id": "NCL000160",
    "all_ids": ["NCL000160"],
    "full_name": "Sarah Stevens",
    "first_name": "Sarah",
    "last_name": "Stevens",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/592.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/592",
    "email": "Sarah.Stevens@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "90",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/592"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "90",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-1883",
      "email": null,
      "address": "2161 Margaret Dr;Mt. Airy, NC 27030",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-1883",
      "email": "Sarah.Stevens@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 419;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:30",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "NCL000393",
    "leg_id": "NCL000393",
    "all_ids": ["NCL000393"],
    "full_name": "Marcia Morey",
    "first_name": "Marcia",
    "last_name": "Morey",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/732.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/732",
    "email": "Marcia.Morey@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "30",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/732"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "30",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-7663",
      "email": null,
      "address": "16 West Jones Street, Rm. 1109;Raleigh, NC 27601-1096",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-7663",
      "email": "Marcia.Morey@ncleg.net",
      "address": "16 West Jones Street, Rm. 1109;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:31",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000298",
    "leg_id": "NCL000298",
    "all_ids": ["NCL000298"],
    "full_name": "Stephen M. Ross",
    "first_name": "Stephen M.",
    "last_name": "Ross",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/664.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/664",
    "email": "Stephen.Ross@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "63",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/664"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "63",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5820",
      "email": null,
      "address": "1314 McCuiston Dr;Burlington, NC 27215",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5820",
      "email": "Stephen.Ross@ncleg.net",
      "address": "16 West Jones Street, Rm. 1229;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:31",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "NCL000341",
    "leg_id": "NCL000341",
    "all_ids": ["NCL000341"],
    "full_name": "Gale Adcock",
    "first_name": "Gale",
    "last_name": "Adcock",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/688.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/688",
    "email": "Gale.Adcock@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "41",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/688"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "41",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5602",
      "email": null,
      "address": "300 Legault Dr;Cary, NC 27513",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5602",
      "email": "Gale.Adcock@ncleg.net",
      "address": "16 West Jones Street, Rm. 1213;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:31",
    "updated_at": "2019-01-24 06:13:20"
  }, {
    "id": "NCL000221",
    "leg_id": "NCL000221",
    "all_ids": ["NCL000221", "NCL000300"],
    "full_name": "Phil Shepard",
    "first_name": "Phil",
    "last_name": "Shepard",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/628.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/628",
    "email": "Phil.Shepard@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "15",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/628"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "15",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-389-6392",
      "email": null,
      "address": "111 Vernon Shepard Ln;Jacksonville, NC 28540",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-9644",
      "email": "Phil.Shepard@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 534;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:31",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "NCL000122",
    "leg_id": "NCL000122",
    "all_ids": ["NCL000122"],
    "full_name": "Linda P. Johnson",
    "first_name": "Linda P.",
    "last_name": "Johnson",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/292.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/292",
    "email": "Linda.Johnson2@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "82",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/292"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "82",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-932-1376",
      "email": null,
      "address": "1205 Berkshire Dr;Kannapolis, NC 28081",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5861",
      "email": "Linda.Johnson2@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 301D;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:32",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "NCL000384",
    "leg_id": "NCL000384",
    "all_ids": ["NCL000384"],
    "full_name": "Donna McDowell White",
    "first_name": "Donna McDowell",
    "last_name": "White",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/728.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/728",
    "email": "Donna.White@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "26",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/728"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "26",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-889-1239",
      "email": null,
      "address": "PO Box 1351;Clayton, NC 27528",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5605",
      "email": "Donna.White@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 306A2;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:32",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "NCL000301",
    "leg_id": "NCL000301",
    "all_ids": ["NCL000301"],
    "full_name": "Michael Speciale",
    "first_name": "Michael",
    "last_name": "Speciale",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/671.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/671",
    "email": "Michael.Speciale@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "3",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/671"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "3",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-635-5326",
      "email": null,
      "address": "803 Stately Pines Rd;New Bern, NC 28560",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5853",
      "email": "Michael.Speciale@ncleg.net",
      "address": "16 West Jones Street, Rm. 1106;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:32",
    "updated_at": "2019-01-24 06:13:33"
  }, {
    "id": "NCL000267",
    "leg_id": "NCL000267",
    "all_ids": ["NCL000267"],
    "full_name": "John R. Bell, IV",
    "first_name": "John R.",
    "last_name": "Bell",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/661.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/661",
    "email": "John.Bell@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "10",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/661"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "10",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3017",
      "email": null,
      "address": "300 N. Salisbury Street, Rm. 301F;Raleigh, NC 27603-5925",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3017",
      "email": "John.Bell@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 301F;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:33",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "NCL000376",
    "leg_id": "NCL000376",
    "all_ids": ["NCL000376"],
    "full_name": "Joe John",
    "first_name": "Joe",
    "last_name": "John",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/722.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/722",
    "email": "Joe.John@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "40",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/722"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "40",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5530",
      "email": null,
      "address": "11800 Black Horse Run;Raleigh, NC 27613",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5530",
      "email": "Joe.John@ncleg.net",
      "address": "16 West Jones Street, Rm. 1013;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:33",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "NCL000128",
    "leg_id": "NCL000128",
    "all_ids": ["NCL000128"],
    "full_name": "David R. Lewis",
    "first_name": "David R.",
    "last_name": "Lewis",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/389.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/389",
    "email": "David.Lewis@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "53",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/389"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "53",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "910-897-8100",
      "email": null,
      "address": "PO Box 1152;Dunn, NC 28335",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3015",
      "email": "David.Lewis@ncleg.net",
      "address": "16 West Jones Street, Rm. 2301;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:33",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000269",
    "leg_id": "NCL000269",
    "all_ids": ["NCL000269"],
    "full_name": "Mark Brody",
    "first_name": "Mark",
    "last_name": "Brody",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/663.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/663",
    "email": "Mark.Brody@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "55",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/663"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "55",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-965-6585",
      "email": null,
      "address": "5315 Rocky River Rd;Monroe, NC 28112",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3029",
      "email": "Mark.Brody@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 416B;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:33",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "NCL000155",
    "leg_id": "NCL000155",
    "all_ids": ["NCL000155"],
    "full_name": "Mitchell S. Setzer",
    "first_name": "Mitchell S.",
    "last_name": "Setzer",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/149.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/149",
    "email": "Mitchell.Setzer@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "89",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/149"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "89",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-241-3570",
      "email": null,
      "address": "PO Box 416;Catawba, NC 28609",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-4948",
      "email": "Mitchell.Setzer@ncleg.net",
      "address": "16 West Jones Street, Rm. 2204;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:34",
    "updated_at": "2019-01-24 06:13:31"
  }, {
    "id": "NCL000370",
    "leg_id": "NCL000370",
    "all_ids": ["NCL000370"],
    "full_name": "Destin Hall",
    "first_name": "Destin",
    "last_name": "Hall",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/719.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/719",
    "email": "Destin.Hall@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "87",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/719"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "87",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5931",
      "email": null,
      "address": "606 College Ave SW;Lenoir, NC 28645",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5931",
      "email": "Destin.Hall@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 530;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:34",
    "updated_at": "2019-01-24 06:13:23"
  }, {
    "id": "NCL000380",
    "leg_id": "NCL000380",
    "all_ids": ["NCL000380"],
    "full_name": "Chaz Beasley",
    "first_name": "Chaz",
    "last_name": "Beasley",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/712.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/712",
    "email": "Chaz.Beasley@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "92",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/712"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "92",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5654",
      "email": null,
      "address": "11619 Hophornbeam Ln;Charlotte, NC 28278",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5654",
      "email": "Chaz.Beasley@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 403;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:34",
    "updated_at": "2019-01-24 06:13:34"
  }, {
    "id": "NCL000374",
    "leg_id": "NCL000374",
    "all_ids": ["NCL000374"],
    "full_name": "Cynthia Ball",
    "first_name": "Cynthia",
    "last_name": "Ball",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/711.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/711",
    "email": "Cynthia.Ball@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "49",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/711"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "49",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5860",
      "email": null,
      "address": "1428 Canterbury Rd;Raleigh, NC 27608",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5860",
      "email": "Cynthia.Ball@ncleg.net",
      "address": "16 West Jones Street, Rm. 1004;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:34",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "NCL000114",
    "leg_id": "NCL000114",
    "all_ids": ["NCL000114"],
    "full_name": "Julia C. Howard",
    "first_name": "Julia C.",
    "last_name": "Howard",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/53.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/53",
    "email": "Julia.Howard@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "77",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/53"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "77",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "336-751-3538",
      "email": null,
      "address": "330 S Salisbury St;Mocksville, NC 27028",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5904",
      "email": "Julia.Howard@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 302;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:35",
    "updated_at": "2019-01-24 06:13:19"
  }, {
    "id": "NCL000143",
    "leg_id": "NCL000143",
    "all_ids": ["NCL000143"],
    "full_name": "Tim Moore",
    "first_name": "Tim",
    "last_name": "Moore",
    "suffix": "",
    "photo_url": "http://www.ncleg.net/House/pictures/hiRes/339.jpg",
    "url": "https://www.ncleg.gov/Members/Biography/H/339",
    "email": "Tim.Moore@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "111",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/339"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "111",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-739-1221",
      "email": null,
      "address": "305 East King St;Kings Mountain, NC 28086",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-3451",
      "email": "Tim.Moore@ncleg.net",
      "address": "16 West Jones Street, Rm. 2304;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2018-10-18 14:52:35",
    "updated_at": "2019-01-24 06:13:31"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Lisa Stone Barnes",
    "first_name": "Lisa Stone",
    "last_name": "Barnes",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/735",
    "email": "Lisa.Barnes@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "7",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/735"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "7",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3032",
      "email": null,
      "address": "300 N. Salisbury Street, Rm. 531;Raleigh, NC 27603-5925",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3032",
      "email": "Lisa.Barnes@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 531;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:18",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Brandon Lofton",
    "first_name": "Brandon",
    "last_name": "Lofton",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/751",
    "email": "Brandon.Lofton@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "104",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/751"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "104",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3009",
      "email": null,
      "address": "921 Dacavin Dr;Charlotte, NC 28226",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3009",
      "email": "Brandon.Lofton@ncleg.net",
      "address": "16 West Jones Street, Rm. 1317;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:18",
    "updated_at": "2019-01-24 06:13:18"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "C. Ray Russell",
    "first_name": "C. Ray",
    "last_name": "Russell",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/753",
    "email": "Ray.Russell@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "93",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/753"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "93",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-7727",
      "email": null,
      "address": "105 Freds Dr;Boone, NC 28607",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-7727",
      "email": "Ray.Russell@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 602;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:21",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Sydney Batch",
    "first_name": "Sydney",
    "last_name": "Batch",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/736",
    "email": "Sydney.Batch@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "37",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/736"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "37",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-2962",
      "email": null,
      "address": "120 Penmarc Dr, Suite 101;Raleigh, NC 27603",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-2962",
      "email": "Sydney.Batch@ncleg.net",
      "address": "16 West Jones Street, Rm. 1209;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:21",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Chris Humphrey",
    "first_name": "Chris",
    "last_name": "Humphrey",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/747",
    "email": "Chris.Humphrey@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "12",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/747"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "12",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5995",
      "email": null,
      "address": "PO Box 601;La Grange, NC 28551",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5995",
      "email": "Chris.Humphrey@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 632;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:21",
    "updated_at": "2019-01-24 06:13:21"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Bobby Hanig",
    "first_name": "Bobby",
    "last_name": "Hanig",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/745",
    "email": "Bobby.Hanig@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "6",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/745"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "6",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5906",
      "email": null,
      "address": "102 Orchard Ln;Powells Point, NC 27966",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5906",
      "email": "Bobby.Hanig@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 604;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:21",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Julie von Haefen",
    "first_name": "Julie",
    "last_name": "von Haefen",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/758",
    "email": "Julie.vonHaefen@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "36",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/758"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "36",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-0795",
      "email": null,
      "address": "PO Box 267;Apex, NC 27502",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-0795",
      "email": "Julie.vonHaefen@ncleg.net",
      "address": "16 West Jones Street, Rm. 1311;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:22",
    "updated_at": "2019-01-24 06:13:22"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Ashton Wheeler Clemmons",
    "first_name": "Ashton Wheeler",
    "last_name": "Clemmons",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/739",
    "email": "Ashton.Clemmons@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "57",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/739"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "57",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5781",
      "email": null,
      "address": "1607 Beechtree Rd;Greensboro, NC 27408",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5781",
      "email": "Ashton.Clemmons@ncleg.net",
      "address": "16 West Jones Street, Rm. 1211;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:23",
    "updated_at": "2019-01-24 06:13:23"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Carson Smith",
    "first_name": "Carson",
    "last_name": "Smith",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/755",
    "email": "Carson.Smith@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "16",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/755"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "16",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-9664",
      "email": null,
      "address": "47 Hidden Bluff Trl;Hampstead, NC 28443",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-9664",
      "email": "Carson.Smith@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 633;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:24",
    "updated_at": "2019-01-24 06:13:24"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Raymond E. Smith, Jr.",
    "first_name": "Raymond E.",
    "last_name": "Smith",
    "suffix": "Jr.",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/757",
    "email": "Raymond.Smith@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "21",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/757"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "21",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5863",
      "email": null,
      "address": "16 West Jones Street, Rm. 2223;Raleigh, NC 27601-1096",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5863",
      "email": "Raymond.Smith@ncleg.net",
      "address": "16 West Jones Street, Rm. 2223;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:25",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Edward C. Goodwin",
    "first_name": "Edward C.",
    "last_name": "Goodwin",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/744",
    "email": "Edward.Goodwin@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "1",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/744"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "1",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-482-8168",
      "email": null,
      "address": "1015 Macedonia Rd;Edenton, NC 27932",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-0010",
      "email": "Edward.Goodwin@ncleg.net",
      "address": "16 West Jones Street, Rm. 2217;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:25",
    "updated_at": "2019-01-24 06:13:25"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Kandie D. Smith",
    "first_name": "Kandie D.",
    "last_name": "Smith",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/756",
    "email": "Kandie.Smith@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "8",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/756"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "8",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3023",
      "email": null,
      "address": "PO Box 1832;Greenville, NC 27835",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3023",
      "email": "Kandie.Smith@ncleg.net",
      "address": "16 West Jones Street, Rm. 1315;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:26",
    "updated_at": "2019-01-24 06:13:26"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Christy Clark",
    "first_name": "Christy",
    "last_name": "Clark",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/738",
    "email": "Christy.Clark@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "98",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/738"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "98",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5828",
      "email": null,
      "address": "3902 Conner Glenn Dr;Huntersville, NC 28078",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5828",
      "email": "Christy.Clark@ncleg.net",
      "address": "16 West Jones Street, Rm. 1319;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:27",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Allison A. Dahle",
    "first_name": "Allison A.",
    "last_name": "Dahle",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/740",
    "email": "Allison.Dahle@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "11",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/740"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "11",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5755",
      "email": null,
      "address": "16 West Jones Street, Rm. 1015;Raleigh, NC 27601-1096",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5755",
      "email": "Allison.Dahle@ncleg.net",
      "address": "16 West Jones Street, Rm. 1015;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:27",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Wesley Harris",
    "first_name": "Wesley",
    "last_name": "Harris",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/746",
    "email": "Wesley.Harris@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "105",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/746"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "105",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-437-2618",
      "email": null,
      "address": "3570 Toringdon Way, Apt, 4026;Charlotte, NC 28277",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5886",
      "email": "Wesley.Harris@ncleg.net",
      "address": "16 West Jones Street, Rm. 1321;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:27",
    "updated_at": "2019-01-24 06:13:27"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Wayne Sasser",
    "first_name": "Wayne",
    "last_name": "Sasser",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/754",
    "email": "Clayton.Sasser@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "67",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/754"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "67",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-982-8003",
      "email": null,
      "address": "29013 Jordan Pond Dr;Albemarle, NC 28001",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5908",
      "email": "Clayton.Sasser@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 418A;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:28",
    "updated_at": "2019-01-24 06:13:28"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Zack Hawkins",
    "first_name": "Zack",
    "last_name": "Hawkins",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/742",
    "email": "Zack.Hawkins@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "31",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/742"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "31",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-2528",
      "email": null,
      "address": "PO Box 829;Durham, NC 27702",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-2528",
      "email": "Zack.Hawkins@ncleg.net",
      "address": "16 West Jones Street, Rm. 1307;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:28",
    "updated_at": "2019-01-24 06:13:28"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Keith Kidwell",
    "first_name": "Keith",
    "last_name": "Kidwell",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/749",
    "email": "Keith.Kidwell@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "79",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/749"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "79",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5881",
      "email": null,
      "address": "53 Elks Rd;Chocowinity, NC 27817",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5881",
      "email": "Keith.Kidwell@ncleg.net",
      "address": "16 West Jones Street, Rm. 2213;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:28",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Joe Sam Queen",
    "first_name": "Joe Sam",
    "last_name": "Queen",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/669",
    "email": "Joe.Queen@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "119",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/669"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "119",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "828-452-1688",
      "email": null,
      "address": "209 Hillview Cir.;Waynesville, NC 28786",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3005",
      "email": "Joe.Queen@ncleg.net",
      "address": "16 West Jones Street, Rm. 1002;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:29",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Steve Jarvis",
    "first_name": "Steve",
    "last_name": "Jarvis",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/748",
    "email": "Steve.Jarvis@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "80",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/748"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "80",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-2526",
      "email": null,
      "address": "470 Old NC Hwy 75;Lexington, NC 27292",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-2526",
      "email": "Steve.Jarvis@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 306C;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:29",
    "updated_at": "2019-01-24 06:13:29"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "James D. Gailliard",
    "first_name": "James D.",
    "last_name": "Gailliard",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/743",
    "email": "James.Gailliard@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "25",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/743"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "25",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "252-822-4007",
      "email": null,
      "address": "9121 West Mount Dr;Rocky Mount, NC 27803",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5802",
      "email": "James.Gailliard@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 536;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:29",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Nasif Majeed",
    "first_name": "Nasif",
    "last_name": "Majeed",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/752",
    "email": "Nasif.Majeed@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "99",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/752"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "99",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-536-1153",
      "email": null,
      "address": "5401 Rupert Ln;Charlotte, NC 28215",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5606",
      "email": "Nasif.Majeed@ncleg.net",
      "address": "16 West Jones Street, Rm. 1008;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:30",
    "updated_at": "2019-01-24 06:13:30"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Rachel Hunt",
    "first_name": "Rachel",
    "last_name": "Hunt",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/759",
    "email": "Rachel.Hunt@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "103",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/759"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "103",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-542-1650",
      "email": null,
      "address": "3310 Windbluff Dr;Charlotte, NC 28277",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5800",
      "email": "Rachel.Hunt@ncleg.net",
      "address": "16 West Jones Street, Rm. 1111;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:32",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Carolyn G. Logan",
    "first_name": "Carolyn G.",
    "last_name": "Logan",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/750",
    "email": "Carolyn.Logan@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "101",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/750"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "101",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "704-395-1540",
      "email": null,
      "address": "7216 Tall Tree Ln;Charlotte, NC 28214",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-2530",
      "email": "Carolyn.Logan@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 603;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:32",
    "updated_at": "2019-01-24 06:13:32"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Jerry Carter",
    "first_name": "Jerry",
    "last_name": "Carter",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/737",
    "email": "Jerry.Carter@ncleg.net",
    "party": "Republican",
    "chamber": "lower",
    "district": "65",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/737"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "65",
      "chamber": "lower",
      "state": "nc",
      "party": "Republican",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-733-5779",
      "email": null,
      "address": "303 Grady Rd;Reidsville, NC 27320",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-733-5779",
      "email": "Jerry.Carter@ncleg.net",
      "address": "300 N. Salisbury Street, Rm. 418B;Raleigh, NC 27603-5925",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:33",
    "updated_at": "2019-01-24 06:13:33"
  }, {
    "id": "~not available~",
    "leg_id": "~not available~",
    "all_ids": ["~not available~"],
    "full_name": "Terence Everitt",
    "first_name": "Terence",
    "last_name": "Everitt",
    "suffix": "",
    "photo_url": "",
    "url": "https://www.ncleg.gov/Members/Biography/H/741",
    "email": "Terence.Everitt@ncleg.net",
    "party": "Democratic",
    "chamber": "lower",
    "district": "35",
    "state": "nc",
    "sources": [{
      "url": "https://www.ncleg.gov/Members/Biography/H/741"
    }],
    "active": true,
    "roles": [{
      "term": "2017-2018",
      "district": "35",
      "chamber": "lower",
      "state": "nc",
      "party": "Democratic",
      "type": "member",
      "start_date": null,
      "end_date": null
    }],
    "offices": [{
      "name": "District Office",
      "fax": null,
      "phone": "919-715-3010",
      "email": null,
      "address": "16 West Jones Street, Rm. 1301;Raleigh, NC 27601-1096",
      "type": "district"
    }, {
      "name": "Capitol Office",
      "fax": null,
      "phone": "919-715-3010",
      "email": "Terence.Everitt@ncleg.net",
      "address": "16 West Jones Street, Rm. 1301;Raleigh, NC 27601-1096",
      "type": "capitol"
    }],
    "old_roles": {},
    "middle_name": "",
    "country": "us",
    "level": "state",
    "created_at": "2019-01-24 06:13:34",
    "updated_at": "2019-01-24 06:13:34"
  }]
};