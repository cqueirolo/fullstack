//------------------------General Stats------------------------//
function createGeneralTable(statistics) {
  var table = document.getElementById('general-stat');
  var tableElement = '<thead><tr><th>Party</th><th>N° of Reps</th><th>% Voted w/ Party</th></tr></thead>';
  tableElement += '<tbody>';
  tableElement += '<tr><td>Democrats</td><td>' + statistics.members.d.number + '</td><td>' + statistics.members.d.pct_votes_w_party + '</td></tr>';
  tableElement += '<tr><td>Republicans</td><td>' + statistics.members.r.number + '</td><td>' + statistics.members.r.pct_votes_w_party + '</td></tr>';
  tableElement += '<tr><td>Independents</td><td>' + statistics.members.i.number + '</td><td>' + statistics.members.i.pct_votes_w_party + '</td></tr>';
  tableElement += '<tr><td>Total</td><td>' + statistics.members.total_number + '</td><td>' + statistics.members.avg_pct_votes_w_party + '</td></tr>';
  tableElement += '</tbody>';
  table.innerHTML = tableElement;
}
//------------------------Party Loyalty------------------------//
function createLeastLoyalTable(statistics) {
  var table = document.getElementById('least-loyal');
  var tableElement = '<thead><tr><th>Name</th><th>Party</th><th>N° Party Votes</th><th>% Party Votes</th></tr></thead>';
  tableElement += '<tbody>';
  statistics.ranking.least_often_vote_w_party.forEach(function (member) {
    tableElement += '<tr>';
    if (member.middle_name === null) {
      tableElement += '<td>' + member.last_name + ', ' + member.first_name + '</a></td>';
    } else {
      tableElement += '<td>' + member.last_name + ', ' + member.first_name + ' ' + member.middle_name + '</a></td>';
    }
    tableElement += '<td>' + member.party + '</td>';
    tableElement += '<td>' + member.total_votes + '</td>';
    tableElement += '<td>' + member.votes_with_party_pct + '</td></tr>';
  });
  tableElement += '</tbody>';
  table.innerHTML = tableElement;
}

function createMostLoyalTable(statistics) {
  var table = document.getElementById('most-loyal');
  var tableElement = '<thead><tr><th>Name</th><th>Party</th><th>N° Party Votes</th><th>% Party Votes</th></tr></thead>';
  statistics.ranking.most_often_vote_w_party.forEach(function (member) {
    tableElement += '<tr>';
    if (member.middle_name === null) {
      tableElement += '<td>' + member.last_name + ', ' + member.first_name + '</a></td>';
    } else {
      tableElement += '<td>' + member.last_name + ', ' + member.first_name + ' ' + member.middle_name + '</a></td>';
    }
    tableElement += '<td>' + member.party + '</td>';
    tableElement += '<td>' + member.total_votes + '</td>';
    tableElement += '<td>' + member.votes_with_party_pct + '</td></tr>';
  });
  tableElement += '</tbody>';
  table.innerHTML = tableElement;
}
//---------------------------------------------------------------//
promise.then(function (result) {
  var members = result.results[0].members;
  loadStatisticsJson(members, statistics);
  createGeneralTable(statistics);
  createLeastLoyalTable(statistics);
  createMostLoyalTable(statistics);
});
