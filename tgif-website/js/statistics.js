//Función de carga de datos en el JSON//
function createStatisticsJson(members) {
    //JSON//
  var statistics = {
    'members': {
      'd': {
        'number': 0,
        'pct_votes_w_party': 0
      },
      'r': {
        'number': 0,
        'pct_votes_w_party': 0
      },
      'i': {
        'number': 0,
        'pct_votes_w_party': 0
      },
      'total_number': 0,
      'avg_pct_votes_w_party': 0
    },
    'ranking': {
      'most_often_votes_w_party': [],
      'least_often_votes_w_party': [],
      'missed_most_votes': [],
      'missed_least_votes': []
    }
  };
  let aux = [];
  aux = parseMemberList(members, statistics);
  let d = aux[0];
  let r = aux[1];
  let i = aux[2];
  numberOfMembers(d, r, i, statistics);
  votesWPartyPct(d, r, i, statistics);
  let pct = 10;
  //Menor cantidad de votos perdidos
  statistics.ranking.missed_least_votes = missedLeastVotes(d, r, i, pct);
  //Mayor cantidad de votos perdidos
  statistics.ranking.missed_most_votes = missedMostVotes(d, r, i, pct);
  //Menor cantidad de votos para su partido
  statistics.ranking.least_often_votes_w_party = leastOftenVotesWParty(d, r, i, pct);
  //Mayor cantidad de votos para su partido
  statistics.ranking.most_often_votes_w_party = mostOftenVotesWParty(d, r, i, pct);
  return statistics;
}

function parseMemberList(members) {
  var d = [];
  var r = [];
  var i = [];
  members.forEach(function (member) {
    if (member.party === "D") {
      d.push(member);
    } else if (member.party === "R") {
      r.push(member);
    } else {
      i.push(member);
    }
  });
  return [d, r, i];
}
//Cantidad de miembros
function numberOfMembers(d, r, i, statistics) {
  statistics.members.d.number = d.length;
  statistics.members.r.number = r.length;
  statistics.members.i.number = i.length;
  statistics.members.total_number = d.length + r.length + i.length;
}
//Porcentaje de votos para su partido
function votesWPartyPct(d, r, i, statistics) {
  var auxD = 0;
  d.forEach(function (member) {
    auxD += member.votes_with_party_pct;
  });
  statistics.members.d.pct_votes_w_party = auxD / statistics.members.d.number;
  var auxR = 0;
  r.forEach(function (member) {
    auxR += member.votes_with_party_pct;
  });
  statistics.members.r.pct_votes_w_party = auxR / statistics.members.r.number;
  var auxI = 0;
  if (i.length === 0) {
    statistics.members.i.pct_votes_w_party = 0;
  } else {
    i.forEach(function (member) {
      auxI += member.votes_with_party_pct;
    });
    statistics.members.i.pct_votes_w_party = auxI / statistics.members.i.number;
  }
  statistics.members.avg_pct_votes_w_party = (auxD + auxR + auxI) / statistics.members.total_number;
}
//Rankings
function missedLeastVotes(d, r, i, pct) {
  //ordena de menor a mayor segun cantidad de votos perdidos
  let members = d.concat(r, i).sort((a, b) => (a.missed_votes_pct > b.missed_votes_pct) ? 1 : -1);
  let missedLeastVotesList = [];
  let j = 0;
  while (missedLeastVotesList.length * 100 / members.length <= pct) {
    missedLeastVotesList.push(members[j]);
    j += 1;
  }
  while (missedLeastVotesList[j - 1].missed_votes_pct === members[j].missed_votes_pct) {
    missedLeastVotesList.push(members[j]);
    j += 1;
  }
  return missedLeastVotesList;
}

function missedMostVotes(d, r, i, pct) {
  //ordena de mayor a menor segun cantidad de votos perdidos
  let members = d.concat(r, i).sort((a, b) => (a.missed_votes_pct < b.missed_votes_pct) ? 1 : -1);
  let missedMostVotesList = [];
  let j = 0;
  while (missedMostVotesList.length * 100 / members.length <= pct) {
    missedMostVotesList.push(members[j]);
    j += 1;
  }
  while (missedMostVotesList[j - 1].missed_votes_pct === members[j].missed_votes_pct) {
    missedMostVotesList.push(members[j]);
    j += 1;
  }
  return missedMostVotesList;
}

function leastOftenVotesWParty(d, r, i, pct) {
  //ordena de menor a mayor segun cantidad de votos perdidos
  let members = d.concat(r, i).sort((a, b) => (a.votes_with_party_pct > b.votes_with_party_pct) ? 1 : -1);
  let leastOftenVotesWPartyList = [];
  let j = 0;
  while (leastOftenVotesWPartyList.length * 100 / members.length <= pct) {
    leastOftenVotesWPartyList.push(members[j]);
    j += 1;
  }
  while (leastOftenVotesWPartyList[j - 1].missed_votes_pct === members[j].missed_votes_pct) {
    leastOftenVotesWPartyList.push(members[j]);
    j += 1;
  }
  return leastOftenVotesWPartyList;
}

function mostOftenVotesWParty(d, r, i, pct) {
  //ordena de mayor a menor segun cantidad de votos hechos al partido
  let members = d.concat(r, i).sort((a, b) => (a.votes_with_party_pct < b.votes_with_party_pct) ? 1 : -1);
  let mostOftenVotesWPartyList = [];
  let j = 0;
  while (mostOftenVotesWPartyList.length * 100 / members.length <= pct) {
    mostOftenVotesWPartyList.push(members[j]);
    j += 1;
  }
  while (mostOftenVotesWPartyList[j - 1].missed_votes_pct === members[j].missed_votes_pct) {
    mostOftenVotesWPartyList.push(members[j]);
    j += 1;
  }
  return mostOftenVotesWPartyList;
}
