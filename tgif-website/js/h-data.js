//------------------------------------------------------------------------------------------------------------------------------------------------------------------//
//Si el JSON esta cargado, se dibuja la tabla
promise.then(function (result) {
  var members = result.results[0].members;
  createTable(members);
  setOnchange(members);
});
//------------------------------------------------------------------------------------------------------------------------------------------------------------------/
function createTable(members) {
  var table = document.getElementById('house-data');
  var tableElement = createTableElement(members);
  table.innerHTML = tableElement;
}

function createTableElement(members) {
  var tableElement = '<thead><tr><th>Representative</th><th>Party Affiliation</th><th>State</th><th>Seniority</th><th>Votes with Party</th></tr></thead>';
  tableElement += '<tbody>';
  members.forEach(function (member) {
    tableElement += '<tr>';
    if (member.middle_name === null) {
      tableElement += '<td><a href=' + member.url + ' target=\'_blank\'>' + member.last_name + ', ' + member.first_name + '</a></td>';
    } else {
      tableElement += '<td><a href=' + member.url + '>' + member.last_name + ', ' + member.first_name + ' ' + member.middle_name + '</a></td>';
    }
    tableElement += '<td>' + member.party + '</td>';
    tableElement += '<td>' + member.state + '</td>';
    tableElement += '<td>' + member.seniority + '</td>';
    tableElement += '<td>' + member.votes_with_party_pct + '</td></tr>';
  });
  tableElement += '</tbody>';
  return tableElement;
}

function getCheckedBoxes(checkboxesList) {
  var checkboxesChecked = [];
  for (var i = 0; i < checkboxesList.length; i++) {
    if (checkboxesList[i].checked) {
      checkboxesChecked.push(checkboxesList[i].value);
    }
  }
  return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

function filterMembers(members, selectedParties, selectedState) {
  var filteredMemberList = [];
  members.forEach(function (member) {
    if (selectedState === 'All') {
      if (selectedParties && selectedParties.includes(member.party)) {
        filteredMemberList.push(member);
      }
    } else if (selectedState === member.state) {
      if (selectedParties && selectedParties.includes(member.party)) {
        filteredMemberList.push(member);
      } 
    }
  });
  return filteredMemberList;
}

function setOnchange(members) {
  var checkboxes = document.getElementsByName('party');
  var dropdown = document.getElementById('state');
  var selectedParties = getCheckedBoxes(checkboxes);
  var selectedState = dropdown.value;
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].onchange = function () {
      selectedParties = getCheckedBoxes(checkboxes);
      createTable(filterMembers(members, selectedParties, selectedState));
    }
  }
  dropdown.onchange = function () {
    selectedState = dropdown.value;
    createTable(filterMembers(members, selectedParties, selectedState));
  }
}
