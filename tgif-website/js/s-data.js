//------------------------------------------------------------------------------------------------------------------------------------------------------------------//
var tableVue = new Vue({
  el: '#senate-data',
  data: {
    senators: []
  }
});
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//
function getCheckedBoxes(checkboxesList) {
  var checkboxesChecked = [];
  for (var i = 0; i < checkboxesList.length; i++) {
    if (checkboxesList[i].checked) {
      checkboxesChecked.push(checkboxesList[i].value);
    }
  }
  return checkboxesChecked.length > 0 ? checkboxesChecked : null;
}

function filterMembers(members, selectedParties, selectedState) {
  var filteredMemberList = [];
  members.forEach(function (member) {
    if (selectedState === 'All') {
      if (selectedParties && selectedParties.includes(member.party)) {
        filteredMemberList.push(member);
      } 
    } else if (selectedState === member.state) {
      if (selectedParties && selectedParties.includes(member.party)) {
        filteredMemberList.push(member);
      } 
    }
  });
  return filteredMemberList;
}

function setOnchange(members, tableVue) {
  var checkboxes = document.getElementsByName('party');
  var dropdown = document.getElementById('state');
  var selectedParties = getCheckedBoxes(checkboxes);
  var selectedState = dropdown.value;
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].onchange = function () {
      selectedParties = getCheckedBoxes(checkboxes);
      tableVue.senators = filterMembers(members, selectedParties, selectedState);
    }
  }
  dropdown.onchange = function () {
    selectedState = dropdown.value;
    tableVue.senators = filterMembers(members, selectedParties, selectedState);
  }
}

//Si el JSON esta cargado, se dibuja la tabla
promise.then(function (result) {
  var members = result.results[0].members;
  tableVue.senators = members;
  setOnchange(members, tableVue);
});
