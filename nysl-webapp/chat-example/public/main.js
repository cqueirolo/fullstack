//atajos al DOM
/*crear cuenta*/
var createAccountFullName = document.getElementById('createAccountFullName');
var createAccountTeam = document.getElementById('createAccountTeam');
var createAccountEmail = document.getElementById('createAccountEmail');
var createAccountPassword = document.getElementById('createAccountPassword');
var createAccountButton = document.getElementById('createAccountButton');
/*iniciar sesión*/
var logInEmail = document.getElementById('logInEmail');
var logInPassword = document.getElementById('logInPassword');
var logInButton = document.getElementById('logInButton');
var logInCheck = document.getElementById('logInCheck');
/*enviar mensaje*/
var chatWindow = document.getElementById('chatWindow');
var chatMessage = document.getElementById('chatMessage');
var chatSendMessageButton = document.getElementById('chatSendMessageButton');
/*id del padre de los elementos del chat, para utilizar como referencia en la base de datos*/
var chatParent = chatSendMessageButton.parentElement.getAttribute('id');

//datos del usuario
var userData = {
  fullName,
  email,
  team
}

//funcion de crear cuenta
function createAccount(fullName, team, email, password, userData) {
  firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error){
    var errorCode = error.code;
    var errorMessage = error.message;
    alert('errorCode\nerrorMessage');
  });
  userData.fullName = fullName;
  userData.email = email;
  userData.team = team;
  console.log(userData);
  return 0;
}

//funcion de iniciar sesion
function logIn(email, password) {
  firebase.auth().signInWithEmailAndPassword().catch(function(error){
    var errorCode = error.code;
    var errorMessage = error.message;
    alert('errorCode\nerrorMessage');
  });
  logInCheck.classList.remove('d-none');
  return 0;
};

//funcion de cerrar sesión
function signOut(){
  logInCheck.classList.add('d-none');
}
//funcion de enviar mensaje
function sendMessage() {}

//detecta inicio de sesión
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
  }
});

//envia mensaje
firebase.database().ref(chatParent).push({

});

//lee mensajes y los escribe en la pantalla
firebase.database().ref(chatParent).on('value',function(snapshot){
  var html = '';
  snapshot.forEach(function(e){

  });
});

//añade EventListeners
window.addEventListener('load', function(){
  createAccountButton.addEventListener('click',function(){});
  logInButton.addEventListener('click',function(){});
  chatSendMessageButton.addEventListener('click',function(){});
}, false);