//games.html//
var app = new Vue({
    el: '#page-container',
    data: {
        games_list: null,
        players_list: null,
        player_info: null,
        player_status: '',
        accounts: {
            username: '',
            password: '',
        }
    },
    methods: {
        //Carga de datos//
        data_load: function() {
            this.$http.get('/api/games').then(function(response) {
                this.games_list_load(response.body.games);
                this.players_list_load(response.body.games);
                this.player_info_load(response.body.player);
                console.log(response.body);
            }).catch(function(response) {
                console.log('Failed to get data from server.');
            });
        },

        games_list_load: function(games) {
            let games_list = new Array();
            games.forEach(function(game) {
                //Estructura de datos para cada juego//
                let game_info = {
                    created: '',
                    id: 0,
                    player1: {
                        gpid: 0,
                        id: 0,
                        username: ''
                    },
                    player2: {
                        gpid: 0,
                        id: 0,
                        username: ''
                    },
                    game_full: false,
                };
                game_info.created = new Date(game.created).toLocaleString();
                //Siempre hay al menos 1 jugador//
                game_info.id = game.id;
                game_info.player1.gpid = game.players[0].gpid;
                game_info.player1.id = game.players[0].id;
                game_info.player1.username = game.players[0].username;
                //Si el juego está lleno, caga los datos//
                if (game.players.length > 1) {
                    game_info.game_full = true;
                    game_info.player2.gpid = game.players[1].gpid;
                    game_info.player2.id = game.players[1].id;
                    game_info.player2.username = game.players[1].username;
                }
                games_list.push(game_info);
            });
            this.games_list = games_list;
        },

        players_list_load: function(games) {
            let players_list = new Array();
            games.forEach(function(game) {
                game.players.forEach(function(player) {
                    //Estructura de datos para cada jugador//
                    let player_info = {
                        id: 0,
                        username: '',
                        played: 0,
                        total: 0,
                        won: 0,
                        tied: 0,
                        lost: 0,
                    };
                    if (!players_list.some(player_info => player_info.id === player.id)) {
                        //El jugador actual aún no está en la lista//
                        player_info.id = player.id;
                        player_info.username = player.username;
                        players_list.push(player_info);
                    }
                });
                game.scores.forEach(function(score) {
                    if (score !== null) {
                        //Se busca el jugador correspondiente a la puntuación actual//
                        let index = players_list.findIndex(player => player.id === score.player);
                        if (index !== -1) {
                            players_list[index].total += score.score;
                            players_list[index].played += 1;
                            if (score.score === 1.0) {
                                players_list[index].won += 1;
                            }
                            if (score.score === 0.5) {
                                players_list[index].tied += 1;
                            }
                            else {
                                players_list[index].lost += 1;
                            }
                        }
                    }
                });
            });
            //Se ordenan los jugadores según puntaje//
            players_list.sort(function(a, b) {
                return b.total - a.total;
            });
            this.players_list = players_list;
        },
        player_info_load: function(player) {
            this.player_info = player;
            if (player !== null) {
                this.player_status = 'connected';
            }
            else {
                this.player_status = 'disconnected';
            }
        },

        //Manejo de cuentas//
        login: function() {
            let data = {username: this.accounts.username, password: this.accounts.password}; 
            if (data.username !== '' && data.password !== '') {
                this.$http.post('/api/login', data, {emulateJSON: true}).then(function() {
                    console.log("Logged In.");
                    this.clean_account_info();
                    this.data_load();
                }).catch(function() {
                    console.log("Failed at login attempt.");
                    this.clean_account_info();
                });
            }
        },

        register: function() {
            let data = {username: this.accounts.username, password: this.accounts.password}; 
            if (data.username !== '' && data.password !== '') {
                this.$http.post('/api/players', data, {emulateJSON: true}).then(function() {
                    alert("Account Created.");
                    this.clean_account_info();
                }).catch(function() {
                    console.log("Failed to create account.");
                });
            }
            else {
                alert("Complete both fields");
                this.clean_account_info();
            }
        },

        logout: function() {
            this.$http.get('/api/logout').then(function() {
                alert("Logged out.");
                this.data_load();
            }).catch(function() {
                alert("Failed to Log out.");
            });
        },

        create_game: function() {
            this.$http.post('/api/games').then(function(response) {
                alert('Game created: Redirecting to ship placement.');
                window.location.href = '/web/placement.html?gp'+response.body.gpid;
            }).catch(function(response) {
                alert('Failed at creating game.');
            });
        },

        continue_game: function(gpid) {
            alert('Returning to game.');
            window.location.href = '/web/game.html?gp'+gpid;
        },

        join_game: function(gameid) {
            this.$http.post("/api/game/"+gameid+"/players").then(function(response) {
                alert('Joining game: Redirecting to ship placement');
                window.location.href = '/web/placement.html?gp'+response.body.gpid;
            }).catch(function(response) {
                alert('Failed to Join game: Game is full.');
            });
        },

        clean_account_info: function() {
            this.accounts.username = '';
            this.accounts.password = '';
        },
    },

    created() {
        this.data_load();
        //Recarga los datos del servidor cada 10 segundos.
        this.interval = setInterval(() => this.data_load(), 10000);
    }
});
