//Vue Instance//
var app = new Vue({
    el: '#page-container',
    data: {
        player_info: {
            id: 0,
            gpid: 0,
            username: '',
            game_state: null
        },
        enemy_info: {
            id: 0,
            gpid: 0,
            username: '',
            game_state: null
        },
        game_stats: null,
        salvoes_placed: {
            locations: []
        },
        turn: 0,
        grid: {
            player: [{status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, 
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}],
            enemy: [{status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, 
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
                    {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}]
        }
    },
    methods: {

        game_data_load: function() {
            let gpid = window.location.search.substring(1).slice(2);
            this.$http.get('/api/game_view/' + gpid).then(function(response) {
                this.get_players_info(response.body, gpid);
                this.get_turn(response.body);
                this.grid_load(response.body);
                this.table_load(response.body);
            }).catch(function(response) {
                console.log('Couldn\'t get data');
            });
        },
        //GAME//
        get_players_info: function(data, player_gpid) {
            data.gamePlayers.sort(function(a, b) {
                return a.gpid - b.gpid;
            });
            //El gameplayer es el creador si hay uno solo, y si hay dos es el cuyo gpid coincide con la url.
            if (data.gamePlayers.length == 1) {
                this.player_info.id = data.gamePlayers[0].id;
                this.player_info.gpid = data.gamePlayers[0].gpid;
                this.player_info.username = data.gamePlayers[0].username;
                this.player_info.game_state = data.gamePlayers[0].gameState;
                this.enemy_info.id = 0;
                this.enemy_info.gpid = 0;
                this.enemy_info.username = '...';
                this.enemy_info.game_state = '';
            }
            else {
                if (data.gamePlayers[0].gpid == player_gpid) {
                    this.player_info.id = data.gamePlayers[0].id;
                    this.player_info.gpid = data.gamePlayers[0].gpid;
                    this.player_info.username = data.gamePlayers[0].username;
                    this.player_info.game_state = data.gamePlayers[0].gameState;
                    this.enemy_info.id = data.gamePlayers[1].id;
                    this.enemy_info.gpid = data.gamePlayers[1].gpid;
                    this.enemy_info.username = data.gamePlayers[1].username;
                    this.enemy_info.game_state = data.gamePlayers[1].gameState;
                }
                else {
                    this.player_info.id = data.gamePlayers[1].id;
                    this.player_info.gpid = data.gamePlayers[1].gpid;
                    this.player_info.username = data.gamePlayers[1].username;
                    this.player_info.game_state = data.gamePlayers[1].gameState;
                    this.enemy_info.id = data.gamePlayers[0].id;
                    this.enemy_info.gpid = data.gamePlayers[0].gpid;
                    this.enemy_info.username = data.gamePlayers[0].username;
                    this.enemy_info.game_state = data.gamePlayers[0].gameState;
                }
            }        
        },

        grid_load: function(data) {
            //Ordena el array de salvoes segun el turno (menor a mayor)
            data.salvoes.sort(function(a, b) {
                return a.turn - b.turn;
            });
            //Dibuja los barcos
            data.ships.forEach(function(ship) {
                ship.locations.forEach(function(location) {
                    this.app.grid.player[this.app.to_num_format(location)].status = 'ship';
                });
            });
            //Dibuja los salvos, los hits y los sinks
            data.salvoes.forEach(function(salvo) {
                salvo.locations.forEach(function(location) {
                    if (salvo.player == this.app.player_info.id) {
                        this.app.grid.enemy[this.app.to_num_format(location)].status = 'salvo';
                    }
                });
                salvo.hits.forEach(function(hit) {
                    if (salvo.player == this.app.player_info.id) {
                        this.app.grid.enemy[this.app.to_num_format(hit)].status = 'hit';
                    }
                    else {
                        this.app.grid.player[this.app.to_num_format(hit)].status = 'hit';
                    }
                });
                salvo.sinks.forEach(function(sink) {
                    sink.locations.forEach(function(location) {
                        if (salvo.player == this.app.player_info.id) {
                            this.app.grid.enemy[this.app.to_num_format(location)].status = 'sunk';
                        }
                        else {
                            this.app.grid.player[this.app.to_num_format(location)].status = 'sunk';
                        }
                    });
                });
            });
        },

        table_load: function(data) {
            //Crea un array con los datos que se utilizaran en la tabla.
            //Por cada salvo disparado, se guardan los detalles y al finalizar se ordena segun turno y segun gpid.
            let game_stats = new Array();
            data.salvoes.forEach(function(salvo) {
                let stats = {
                    turn: 0,
                    player: '',
                    gpid: 0,
                    salvoes_fired: null,
                    hits: 0,
                    sinks: 0,
                    sunken_ships: null
                };
                if (this.app.player_info.id == salvo.player) {
                    stats.player = this.app.player_info.username;
                    stats.gpid = this.app.player_info.gpid;
                }
                else {
                    stats.player = this.app.enemy_info.username;
                    stats.gpid = this.app.enemy_info.gpid;
                }
                stats.turn = salvo.turn;
                stats.salvoes_fired = salvo.locations.length;
                stats.hits = salvo.hits.length;
                stats.sinks = salvo.sinks.length;
                stats.sunken_ships = new Array();
                game_stats.push(stats);
            });
            game_stats.sort(function(a, b) {
                let order = b.turn - a.turn;
                if (order == 0){
                    return b.gpid - a.gpid;
                }
                return order;
            });
            this.game_stats = game_stats;
        },

        to_grid_format: function(num_pos) {
            //Se crea un array auxiliar que contendrá las posiciones formateadas
            let grid_pos = '';
            switch (Math.floor(num_pos / 10)) {
                //La parte entera de la división indica la letra de la coordenada
                case 0:
                    grid_pos += 'A';
                    break;
                case 1:
                    grid_pos += 'B';
                    break;
                case 2:
                    grid_pos += 'C';
                    break;
                case 3:
                    grid_pos += 'D';
                    break;
                case 4:
                    grid_pos += 'E';
                    break;
                case 5:
                    grid_pos += 'F';
                    break;
                case 6:
                    grid_pos += 'G';
                    break;
                case 7:
                    grid_pos += 'H';
                    break;
                case 8:
                    grid_pos += 'I';
                    break;
                case 9:
                    grid_pos += 'J';
                    break;
            }
            switch (num_pos % 10) {
                //La parte decimal de la division indica la parte nuemrica de la coordenada
                case 0:
                    grid_pos += '1';
                    break;
                case 1:
                    grid_pos += '2';
                    break;
                case 2:
                    grid_pos += '3';
                    break;
                case 3:
                    grid_pos += '4';
                    break;
                case 4:
                    grid_pos += '5';
                    break;
                case 5:
                    grid_pos += '6';
                    break;
                case 6:
                    grid_pos += '7';
                    break;
                case 7:
                    grid_pos += '8';
                    break;
                case 8:
                    grid_pos += '9';
                    break;
                case 9:
                    grid_pos += '10';
                    break;
            }
            return grid_pos;
        },

        to_num_format: function(grid_pos) {
            /*A -> 0; B -> 10; C -> 20*/
            /*1 -> 0 ... 10 -> 9*/
            let num_pos = 0;
            switch (grid_pos[0]) {
                case 'A':
                    num_pos += 0;
                    break;
                case 'B':
                    num_pos += 10;
                    break;
                case 'C':
                    num_pos += 20;
                    break;
                case 'D':
                    num_pos += 30;
                    break;
                case 'E':
                    num_pos += 40;
                    break;
                case 'F':
                    num_pos += 50;
                    break;
                case 'G':
                    num_pos += 60;
                    break;
                case 'H':
                    num_pos += 70;
                    break;
                case 'I':
                    num_pos += 80;
                    break;
                case 'J':
                    num_pos += 90;
                    break;
            }
            num_pos += parseInt(grid_pos.slice(1)) - 1;
            return num_pos;
        },

        place_salvo: function(index) {
            if (this.salvoes_placed.locations.length < 5) {
                if (this.grid.enemy[index].status != 'hit' && this.grid.enemy[index].status != 'sunk') {
                    this.salvoes_placed.locations.push(this.to_grid_format(index));
                    this.grid.enemy[index].status = 'salvo';
                }
            }
        },

        reset: function() {
            this.grid.enemy.forEach(function(block) {
                block.status = 'water';
            });
            this.game_data_load();
            this.salvoes_placed.locations = [];
        },

        launch_salvoes: function() {
            let data = {
                turn: this.turn, 
                locations: this.salvoes_placed.locations
            }
            this.$http.post('/api/games/players/'+this.player_info.gpid+'/salvoes', JSON.stringify(data), {emulateJSON: true}).then(function(response) {
                alert('Salvoes launched!.');
                this.salvoes_placed.locations = [];
                this.game_data_load();
            }).catch(function(response) {
                alert('Failed to launch salvoes.');
            });
        },

        get_turn: function(data) {
            let turns_player = new Array();
            let turns_enemy = new Array();
            data.salvoes.forEach(function(salvo) {
                if (salvo.player == this.app.player_info.id) {
                    turns_player.push(salvo.turn);
                }
                else {
                    turns_enemy.push(salvo.turn);
                }
            });
            this.turn = (turns_player.length == 0 ? 1 : Math.max(...turns_player)+1);
        },

        leave: function() {
            alert('Leaving game, going back to lobby');
            window.location.href = '/web/games.html';
        },
    },
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    created() {
        this.game_data_load();
        //Recarga los datos del servidor cada 10 segundos.
        this.interval = setInterval(() => this.game_data_load(), 10000);
    }
});
