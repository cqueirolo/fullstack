//Vue Instance//
var app = new Vue({
    el: '#page-container',
    data: {
        player_info: null,
        player_status: 'disconnected',
        gpid: 0,
        grid: [{status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, 
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'},
              {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}, {status: 'water'}],
        ships: [{name:'Aircraft Carrier', length:5, aligned:'Horizontal', placed:false, location: new Array()}, 
            {name:'Battleship', length:4, aligned:'Horizontal', placed:false, location: new Array()},
            {name:'Submarine', length:3, aligned:'Horizontal', placed:false, location: new Array()},
            {name:'Destroyer', length:3, aligned:'Horizontal', placed:false, location: new Array()},
            {name:'Boat', length:2, aligned:'Horizontal', placed:false, location: new Array()}]            
    },
    methods: {
        load_data: function() {
            //Obtiene el gpid de la URL
            this.gpid = window.location.search.substring(1).slice(2);

            this.$http.get('/api/game_view/' + this.gpid).then(function(response) {
                //Si ya estan los barcos emplazados, se redirige al juego
                if (response.body.ships.length == 5) {
                    window.location.href = '/web/game.html?gp'+this.gpid;
                }
            }).catch(function(response) {
                alert('Failed to get ship data from the server. Going back to lobby.');
                window.location.href = '/web/games.html';
            });
            //Se obtiene la información del jugador
            this.$http.get('/api/games').then(function(response) {
                this.get_player_info(response.body);
            }).catch(function(response) {
                alert('Failed to get player data from the server. Going back to lobby.');
                window.location.href = '/web/games.html';
            });
        },

        get_player_info: function(data) {
            this.player_info = data.player;
            if (data.player !== null) {
                this.player_status = 'connected';
            }
            else {
                this.player_status = 'disconnected';
            }
        },

        place_ship: function(index) {
            //Tengo un array con 5 barcos en total, que son todos los barcos posibles
            for (let i = 0; i < this.ships.length; i++) {
                let ship = this.ships[i];
                if (!ship.placed) {
                    //Si el barco no fué posicionado, se calculan los bloques que ocupa
                    for (let j = 0; j < ship.length; j++) {
                        //Por cada bloque que ocupa el barco, se recorre la grilla 
                        //Horizontal o Verticalmente, según sea el caso
                        if (ship.aligned == 'Horizontal') {
                            //Como la posición es Hroizontal, se suma de a uno
                            var curr_pos = index + j;
                            this.grid[curr_pos].status = 'ship';
                        }
                        else {
                            //Como la posición es Vertical, se suma de a diez
                            var curr_pos = index + j*10;
                            this.grid[curr_pos].status = 'ship';
                        }
                        //Por cada bloque sea agrega su posición a la lista de barcos, en el tipo que corresponda
                        ship.location.push(curr_pos);
                    }
                    //Se indica que el barco ya fué posicionado
                    ship.placed = true;
                    return;
                }
            }
        },

        save_ships: function() {
            //Se parte de la suposición que todos los barcos están posicionados
            var all_placed = true;
            //Se crea el arreglo que será devuelto
            var ships = new Array();
            for (let i = 0; i < this.ships.length; i++) {
                //Se recorren los 5 barcos
                let ship = this.ships[i];
                let ship_info = new Object();
                //Se crea un objeto auxiliar para almacenar los detalles para el servidor
                if (!ship.placed) {
                    //Si el barco actual no fué emplazado, se actualiza el flag, se alerta y se sale
                    all_placed = false;
                    alert('Place all ships first!');
                    break;
                }
                ship_info.type = ship.name;
                //Cada barco tiene un array de posiciones numericas. A continuación se formatea
                ship_info.shipLocations = this.array_to_grid_format(ship.location);
                //Se guarda la info de cada barco con el formato correcto
                ships.push(ship_info);
            }
            //Se obtiene el gpid de la url
            this.gpid = window.location.search.substring(1).slice(2);
            //Se hace un post al servidor con el arreglo de barcos creado
            this.$http.post('/api/games/players/'+this.gpid+'/ships', JSON.stringify(ships), {emulateJSON: true}).then(function(response) {
                //Post exitoso
                alert('Barcos emplazados, redireccionando');
                window.location.href = '/web/game.html?gp'+this.gpid;
            }).catch(function(response) {
                //Post fallido
                console.log(response.body);
            });
            return;
        },

        array_to_grid_format: function(arr) {
            //Se crea un array auxiliar que contendrá las posiciones formateadas
            var arr_grid_format = new Array();
            arr.forEach(function(coordenate) {
                let curr_loc = '';
                switch (Math.floor(coordenate / 10)) {
                    //La parte entera de la división indica la letra de la coordenada
                    case 0:
                        curr_loc += 'A';
                        break;
                    case 1:
                        curr_loc += 'B';
                        break;
                    case 2:
                        curr_loc += 'C';
                        break;
                    case 3:
                        curr_loc += 'D';
                        break;
                    case 4:
                        curr_loc += 'E';
                        break;
                    case 5:
                        curr_loc += 'F';
                        break;
                    case 6:
                        curr_loc += 'G';
                        break;
                    case 7:
                        curr_loc += 'H';
                        break;
                    case 8:
                        curr_loc += 'I';
                        break;
                    case 9:
                        curr_loc += 'J';
                        break;
                }
                switch (coordenate % 10) {
                    //La parte decimal de la division indica la parte nuemrica de la coordenada
                    case 0:
                        curr_loc += '1';
                        break;
                    case 1:
                        curr_loc += '2';
                        break;
                    case 2:
                        curr_loc += '3';
                        break;
                    case 3:
                        curr_loc += '4';
                        break;
                    case 4:
                        curr_loc += '5';
                        break;
                    case 5:
                        curr_loc += '6';
                        break;
                    case 6:
                        curr_loc += '7';
                        break;
                    case 7:
                        curr_loc += '8';
                        break;
                    case 8:
                        curr_loc += '9';
                        break;
                    case 9:
                        curr_loc += '10';
                        break;
                }
                arr_grid_format.push(curr_loc);
            });
            return arr_grid_format;
        },

        reset_ships: function() {
            this.grid.forEach(block => {
                block.status = 'water';
            });
            this.ships.forEach(ship => {
                ship.placed = false;
                ship.aligned = 'Horizontal';
            });
        },

        change_aligned: function(index) {
            this.ships[index].aligned = (this.ships[index].aligned == 'Horizontal' ? 'Vertical' : 'Horizontal');
        },
        
        leave: function() {
            alert('Back to lobby.');
            window.location.href = '/web/games.html';
        }
    },
    created() {
        this.load_data();
    }
});